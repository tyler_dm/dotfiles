" All system-wide defaults are set in $VIMRUNTIME/archlinux.vim (usually just
" /usr/share/vim/vimfiles/archlinux.vim) and sourced by the call to :runtime
" you can find below.  If you wish to change any of those settings, you should
" do it in this file (/etc/vimrc), since archlinux.vim will be overwritten
" everytime an upgrade of the vim packages is performed.  It is recommended to
" make changes after sourcing archlinux.vim since it alters the value of the
" 'compatible' option.

" This line should not be removed as it ensures that various options are
" properly set to work with the Vim-related packages.
runtime! archlinux.vim

" If you prefer the old-style vim functionalty, add 'runtime! vimrc_example.vim'
" Or better yet, read /usr/share/vim/vim80/vimrc_example.vim or the vim manual
" and configure vim to your own liking!

" do not load defaults if ~/.vimrc is missing
"let skip_defaults_vim=1

" allow for language specific rules
filetype plugin on

" enable syntax highlighting
syntax on

" enable line numbers
set number

" enable mouse in all modes
set mouse=a

" enable autoindent
set autoindent

" allow for language specific indentation rules
" these can live in ~/.vim/indent/<language>.vim
filetype indent on

" make tabs the correct length
set tabstop=8

" highlight the current line
set cursorline

" enable the wildmenu for autocompletion of filenames
set wildmenu

" redraw only when necessary
set lazyredraw

" highlight matching braces, brackets, and parentheses
set showmatch

" search while typing
set incsearch

" highlight search matches
set hlsearch
